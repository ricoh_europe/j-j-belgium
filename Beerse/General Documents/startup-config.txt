config-file-header
switch669e59
v2.5.0.83 / RTESLA2.5_930_364_091
CLI v1.0
file SSD indicator plaintext
@
!
unit-type-control-start 
unit-type unit 1 network gi uplink none 
unit-type-control-end 
!
bridge multicast filtering 
voice vlan oui-table add 0001e3 Siemens_AG_phone________
voice vlan oui-table add 00036b Cisco_phone_____________
voice vlan oui-table add 00096e Avaya___________________
voice vlan oui-table add 000fe2 H3C_Aolynk______________
voice vlan oui-table add 0060b9 Philips_and_NEC_AG_phone
voice vlan oui-table add 00d01e Pingtel_phone___________
voice vlan oui-table add 00e075 Polycom/Veritel_phone___
voice vlan oui-table add 00e0bb 3Com_phone______________
no eee enable 
ip multicast-routing igmp-proxy 
bonjour interface range vlan 1
qos map dscp-queue 0 to 1
qos map dscp-queue 9 to 1
qos map dscp-queue 10 to 1
qos map dscp-queue 11 to 1
qos map dscp-queue 12 to 1
qos map dscp-queue 13 to 1
qos map dscp-queue 14 to 1
qos map dscp-queue 15 to 1
qos map dscp-queue 16 to 1
qos map dscp-queue 17 to 1
qos map dscp-queue 18 to 1
qos map dscp-queue 19 to 1
qos map dscp-queue 20 to 1
qos map dscp-queue 21 to 1
qos map dscp-queue 22 to 1
qos map dscp-queue 23 to 1
qos map dscp-queue 24 to 1
qos map dscp-queue 25 to 1
qos map dscp-queue 26 to 1
qos map dscp-queue 27 to 1
qos map dscp-queue 28 to 1
qos map dscp-queue 29 to 1
qos map dscp-queue 30 to 1
qos map dscp-queue 31 to 1
qos map dscp-queue 32 to 1
qos map dscp-queue 33 to 1
qos map dscp-queue 34 to 5
qos map dscp-queue 35 to 1
qos map dscp-queue 36 to 1
qos map dscp-queue 37 to 1
qos map dscp-queue 38 to 1
qos map dscp-queue 39 to 1
qos map dscp-queue 40 to 1
qos map dscp-queue 41 to 1
qos map dscp-queue 42 to 1
qos map dscp-queue 43 to 1
qos map dscp-queue 44 to 1
qos map dscp-queue 45 to 1
qos map dscp-queue 46 to 7
qos map dscp-queue 47 to 1
qos map dscp-queue 48 to 1
qos map dscp-queue 49 to 1
qos map dscp-queue 50 to 1
qos map dscp-queue 51 to 1
qos map dscp-queue 52 to 1
qos map dscp-queue 53 to 1
qos map dscp-queue 54 to 1
qos map dscp-queue 55 to 1
qos map dscp-queue 56 to 8
qos map dscp-queue 57 to 1
qos map dscp-queue 58 to 1
qos map dscp-queue 59 to 1
qos map dscp-queue 60 to 1
qos map dscp-queue 61 to 1
qos map dscp-queue 62 to 1
qos map dscp-queue 63 to 1
hostname switch669e59
username Ricoh password encrypted edca0cefd8e3ca3c2564cf439aca1b145379b41d privilege 15 
snmp-server location "Beerse 2, Bldg 100, Rm 019"
clock timezone J +1
no clock source sntp
no sntp unicast client enable
clock dhcp timezone
!
interface vlan 1
 no ip address dhcp 
 bridge multicast mode ipv4-group 
 ip igmp version 2 
 ip igmp query-interval 30 
!
interface GigabitEthernet1
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet2
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet3
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet4
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet5
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet6
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet7
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet8
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet9
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet10
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet11
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet12
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet13
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet14
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet15
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet16
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet17
 no eee enable 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet18
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet19
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet20
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet21
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet22
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet23
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet24
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet25
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet26
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet27
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
interface GigabitEthernet28
 no eee enable 
 bridge multicast unregistered filtering 
 no eee lldp enable 
 ip igmp version 2 
!
exit
ip igmp snooping
ip igmp snooping vlan 1 
ip igmp snooping vlan 1 forbidden mrouter interface gi1 
ip igmp snooping vlan 1 immediate-leave 
ip igmp snooping vlan 1 static 224.0.1.129 
ip igmp snooping vlan 1 querier 
