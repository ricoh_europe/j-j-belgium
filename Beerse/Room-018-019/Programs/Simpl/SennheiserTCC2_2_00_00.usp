/*******************************************************************************************
  SIMPL+ Module Information
  (Fill in comments below)
*******************************************************************************************/
/*
	Module Name	: SennheiserModule
	
	Comments:
	Version	Date(dd/mm/yy)	Remarks 
	------------------------------------------------------------------------------

    0.00.01     06/11/18    Devolpment
    1.00.00     27/05/19    Initial version
    1.00.01     18/12/19    Cleanup
	1.01.00		24/08(20	Recompile for Crestron Database updates
	2.00.00		02/09/20	Implementing Pub/Sub internally for better communication
*/

/*******************************************************************************************
  Compiler Directives
  (Uncomment and declare compiler directives as needed)
*******************************************************************************************/
#SYMBOL_NAME "SennheiserTCC2 2.00.00"
#HINT "Version 2.00.00"
#HELP_PDF_FILE "SENNHEISER_TCC2 MODULE HELP.pdf"

#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_TRACE
#PRINT_TO_TRACE
#ENCODING_ASCII

#USER_SIMPLSHARP_LIBRARY "SennheiserClient"

#DEFINE_CONSTANT	FALSE	0x00
#DEFINE_CONSTANT	TRUE	0x01


//#DEFINE_CONSTANT	DEBUG						1 							// Debug information for general functions
//#DEFINE_CONSTANT	DEBUG_POLL					1 							// Debug information for polling data
//#DEFINE_CONSTANT	DEBUG_PROGRAM				1 							// Debug information for general program data
//#DEFINE_CONSTANT	DEBUG_RESPONSE				1							// Shows generic response parsing message
//#DEFINE_CONSTANT	DEBUG_CRITICAL				1							// Critical error or notices
//#DEFINE_CONSTANT	DEBUG_TRANSMISSION			1							// Shows generic debug messages for transmitting data
//#DEFINE_CONSTANT	DEBUG_ERROR					1							// Shows error debug messages
//#DEFINE_CONSTANT	ERRORLOG					1

#DEFINE_CONSTANT	VERSION_MAJOR				01
#DEFINE_CONSTANT	VERSION_MINOR				02
#DEFINE_CONSTANT	VERSION_BUG					00

#HELP_BEGIN
	Sennheiser Module
#HELP_END


/*******************************************************************************************
  Include Libraries
  (Uncomment and include additional libraries as needed)
*******************************************************************************************/
// #CRESTRON_LIBRARY ""
// #USER_LIBRARY ""

/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
  (Uncomment and declare inputs and outputs as needed)
*******************************************************************************************/
DIGITAL_INPUT _SKIP_, Subscribe, _SKIP_;
DIGITAL_INPUT Audio_Mute_Switch_On, Audio_Mute_Switch_Off, Audio_Mute_Switch_Toggle, _SKIP_, Audio_Level_Tracking, Audio_Reset;
STRING_INPUT _SKIP_, Pass_Thru_In[256];
// BUFFER_INPUT 


DIGITAL_OUTPUT IsCommunicating, _SKIP_, Audio_Mute_Switch, _SKIP_;

ANALOG_OUTPUT  Audio_LevelDb, Beam_Elevation, Beam_Azimuth, LED_Brightness;
ANALOG_OUTPUT _SKIP_, Audio_Attenuation;

STRING_OUTPUT _SKIP_, Device_Name, Device_Location, Device_Position, Device_Identity_Product, Audio_Source_Detection, Audio_Equ_Preset;
STRING_OUTPUT _SKIP_, Warnings, Errors;
STRING_OUTPUT Pass_Thru_Out;
ANALOG_OUTPUT _SKIP_, Audio_EQ_Level[7];

/*******************************************************************************************
  SOCKETS
  (Uncomment and define socket definitions as needed)
*******************************************************************************************/
// TCP_CLIENT
// TCP_SERVER
// UDP_SOCKET

/*******************************************************************************************
  Parameters
  (Uncomment and declare parameters as needed)
*******************************************************************************************/
STRING_PARAMETER IPAddress[16];

/*******************************************************************************************
  Parameter Properties
  (Uncomment and declare parameter properties as needed)
*******************************************************************************************/
#BEGIN_PARAMETER_PROPERTIES IPAddress
   propValidUnits = unitString; //or unitDecimal|unitHex|unitPercent|unitCharacter|unitTime|unitTicks;
   propDefaultValue = "0.0.0.0";  // or, propDefaultValue = "";
   propShortDescription = "IP Address for Sennheiser Device";
#END_PARAMETER_PROPERTIES
/************************************************************************************/

/*******************************************************************************************
  Structure Definitions
  (Uncomment and define structure definitions as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: struct.myString = "";
*******************************************************************************************/
/*
STRUCTURE MyStruct1
{
};

MyStruct1 struct;
*/

/*******************************************************************************************
  Global Variables
  (Uncomment and declare global variables as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: myString = "";
*******************************************************************************************/

STRING gsReceiveBuffer[1024];
STRING gsSendBuffer[1024];
INTEGER gnProcessBuffer;

INTEGER gnActiveCounter;


SennheiserTCC2Class TCC2Sennheiser;																// Sennheiser simpl# interface

/*******************************************************************************************
  Functions
 --------------------------------------------------------------------------------
  Note:  Functions must be physically placed before the location in
         the code that calls them.
*******************************************************************************************/

/********************************************************************************
 Function : Init
 --------------------------------------------------------------------------------
 Arguments: N/A
 Output   : N/A
 Globals  : N/A
 Calls    : N/A
 Return   : N/A
 Purpose  : The purpose of this is to initialize the module
 Notes    : N/A
 ********************************************************************************/
function Init()																	
{
   	#IF_DEFINED DEBUG
		print("[Init]\n");
	#ENDIF

	print("SennheiserClientModule ver. %.2u.%.2u.%.2u. \n", VERSION_MAJOR, VERSION_MINOR, VERSION_BUG );        

	RegisterEvent (TCC2Sennheiser, CustomSocketReceive, SENNHEISER_SOCKETRECEIVE );
	RegisterEvent (TCC2Sennheiser, CustomSocketCommunicating, SENNHEISER_SOCKETCOMMUNICATING );

	RegisterEvent (TCC2Sennheiser, CustomSocketDeviceName, SENNHEISER_DEVICENAME );
	RegisterEvent (TCC2Sennheiser, CustomSocketDeviceLocation, SENNHEISER_DEVICELOCATION );
	RegisterEvent (TCC2Sennheiser, CustomSocketDevicePosition, SENNHEISER_DEVICEPOSITION );
	RegisterEvent (TCC2Sennheiser, CustomSocketDeviceIdentityProduct, SENNHEISER_DEVICEIDENTITYPRODUCT );


	RegisterEvent (TCC2Sennheiser, CustomSocketAudioOutLevelDb, SENNHEISER_AUDIOOUT1LEVELDB );        
	RegisterEvent (TCC2Sennheiser, CustomSocketAudioEquPreset, SENNHEISER_AUDIOEQUPRESET );        
	RegisterEvent (TCC2Sennheiser, CustomSocketAudioEQLevels, SENNHEISER_AUDIOEQULEVELS );        
	RegisterEvent (TCC2Sennheiser, CustomSocketAudioMute, SENNHEISER_AUDIOMUTESWITCH );        
	RegisterEvent (TCC2Sennheiser, CustomSocketAudioSourceDetection, SENNHEISER_AUDIOSOURCEDETECTION );        
	RegisterEvent (TCC2Sennheiser, CustomSocketAudioAttenutation, SENNHEISER_AUDIOATTENUATION );        

	RegisterEvent (TCC2Sennheiser, CustomSocketBeamElevation, SENNHEISER_BEAMELEVATION );        
	RegisterEvent (TCC2Sennheiser, CustomSocketBeamAzimuth, SENNHEISER_BEAMAZIMUTH );        

	RegisterEvent (TCC2Sennheiser, CustomSocketLEDBrightness, SENNHEISER_LEDBRIGHTNESS );        

	RegisterEvent (TCC2Sennheiser, CustomSocketOSCError, SENNHEISER_OSCERROR );    

	TCC2Sennheiser.Init(IPAddress);        
}

//Subscribe/Unsubscribe from pub sub 
Push Subscribe{TCC2Sennheiser.Subscribe();}
Release Subscribe{TCC2Sennheiser.Unsubscribe();}

PUSH Audio_Mute_Switch_On{TCC2Sennheiser.SetAudioMuteSwitch(1);} 
PUSH Audio_Mute_Switch_Off{TCC2Sennheiser.SetAudioMuteSwitch(0);} 
PUSH Audio_Mute_Switch_Toggle
{
	If(Audio_Mute_Switch){TCC2Sennheiser.SetAudioMuteSwitch(0);}
	Else{TCC2Sennheiser.SetAudioMuteSwitch(1);}
} 
        
PUSH Audio_Level_Tracking{TCC2Sennheiser.SetAudioLevelTracking(1);}
RELEASE Audio_Level_Tracking{TCC2Sennheiser.SetAudioLevelTracking(0);}

PUSH Audio_Reset{TCC2Sennheiser.SetAudioReset();}

CHANGE Pass_Thru_In
{
	#IF_DEFINED DEBUG
		print("[Pass_Thru_In]Sending Data start\n");
	#ENDIF
	TCC2Sennheiser.PassThru(Pass_Thru_In);
	#IF_DEFINED DEBUG
		print("[Pass_Thru_In]Sending Data End\n");
	#ENDIF
}
/********************************************************************************
 Function : SENNHEISER_SOCKETRECEIVE
 --------------------------------------------------------------------------------
 Arguments: N/A
 Output   : N/A
 Globals  : gnEthernetStatus
 Calls    : 
 Return   : 
 Purpose  : 
 Notes    : N/A
 ********************************************************************************/
eventhandler SENNHEISER_SOCKETRECEIVE(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_SOCKETRECEIVE]\n");
	#ENDIF

	gsReceiveBuffer = TCC2Sennheiser.gsReceiveBuffer; // Move the Simpl# data into Simpl+
    Pass_Thru_Out = gsReceiveBuffer;

	#if_defined DEBUG_RESPONSE
		print("gsReceiveBuffer = %s", gsReceiveBuffer);
	#ENDIF
}
/********************************************************************************
 Function : SENNHEISER_SOCKETCOMMUNICATING
 --------------------------------------------------------------------------------
 Arguments: N/A
 Output   : N/A
 Globals  : gnEthernetStatus
 Calls    : 
 Return   : 
 Purpose  : 
 Notes    : N/A
 ********************************************************************************/
eventhandler SENNHEISER_SOCKETCOMMUNICATING(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_SOCKETCOMMUNICATING]\n");
	#ENDIF

	IsCommunicating = TCC2Sennheiser.nCommunicating; // Move the Simpl# data into Simpl+
}

/********************************************************************************
 Function : Device 
 --------------------------------------------------------------------------------
 Arguments: 
 Output   : 
 Globals  : 
 Calls    : 
 Return   : 
 Purpose  :
 Notes    : 
 ********************************************************************************/
eventhandler SENNHEISER_DEVICENAME(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_DEVICENAME]\n");
	#ENDIF

	Device_Name = TCC2Sennheiser.gsDeviceName; // Move the Simpl# data into Simpl+
}
eventhandler SENNHEISER_DEVICELOCATION(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_DEVICELOCATION]\n");
	#ENDIF

	Device_Location = TCC2Sennheiser.gsDeviceLocation; // Move the Simpl# data into Simpl+
}
eventhandler SENNHEISER_DEVICEPOSITION(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_DEVICEPOSITION]\n");
	#ENDIF

	Device_Position = TCC2Sennheiser.gsDevicePosition; // Move the Simpl# data into Simpl+
}
eventhandler SENNHEISER_DEVICEIDENTITYPRODUCT(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_DEVICEIDENTITYPRODUCT]\n");
	#ENDIF

	Device_Identity_Product = TCC2Sennheiser.gsDeviceIdentityProduct; // Move the Simpl# data into Simpl+
}

/********************************************************************************
 Function : Audio 
 --------------------------------------------------------------------------------
 Arguments: 
 Output   : 
 Globals  : 
 Calls    : 
 Return   : 
 Purpose  :
 Notes    : 
 ********************************************************************************/
eventhandler SENNHEISER_AUDIOOUT1LEVELDB(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_AUDIOOUT1LEVELDB]\n");
	#ENDIF

	Audio_LevelDb = TCC2Sennheiser.gnAudioOut1LevelDb; // Move the Simpl# data into Simpl+
}	
eventhandler SENNHEISER_AUDIOMUTESWITCH(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_AUDIOMUTESWITCH]\n");
	#ENDIF

	Audio_Mute_Switch = TCC2Sennheiser.gnAudioMute; // Move the Simpl# data into Simpl+
}	
eventhandler SENNHEISER_AUDIOEQUPRESET(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_AUDIOEQUPRESET]\n");
	#ENDIF

	Audio_Equ_Preset = TCC2Sennheiser.gsAudioEquPreset; // Move the Simpl# data into Simpl+
}	     
eventhandler SENNHEISER_AUDIOEQULEVELS(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_AUDIOEQULEVELS]\n");
	#ENDIF
                    
	Audio_EQ_Level[1] = TCC2Sennheiser.gnAudioEQCustom[0]; // Move the Simpl# data into Simpl+
	Audio_EQ_Level[2] = TCC2Sennheiser.gnAudioEQCustom[1]; // Move the Simpl# data into Simpl+
	Audio_EQ_Level[3] = TCC2Sennheiser.gnAudioEQCustom[2]; // Move the Simpl# data into Simpl+
	Audio_EQ_Level[4] = TCC2Sennheiser.gnAudioEQCustom[3]; // Move the Simpl# data into Simpl+
	Audio_EQ_Level[5] = TCC2Sennheiser.gnAudioEQCustom[4]; // Move the Simpl# data into Simpl+
	Audio_EQ_Level[6] = TCC2Sennheiser.gnAudioEQCustom[5]; // Move the Simpl# data into Simpl+	
	Audio_EQ_Level[7] = TCC2Sennheiser.gnAudioEQCustom[6]; // Move the Simpl# data into Simpl+
}	     
eventhandler SENNHEISER_AUDIOSOURCEDETECTION(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_AUDIOSOURCEDETECTION]\n");
	#ENDIF

	Audio_Source_Detection = TCC2Sennheiser.gsAudioSourceDetection; // Move the Simpl# data into Simpl+
}	     
eventhandler SENNHEISER_AUDIOATTENUATION(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_AUDIOATTENUATION]\n");
	#ENDIF

	Audio_Attenuation = TCC2Sennheiser.gnAudioAttenuation; // Move the Simpl# data into Simpl+
}	     
/********************************************************************************
 Function : Beam 
 --------------------------------------------------------------------------------
 Arguments: 
 Output   : 
 Globals  : 
 Calls    : 
 Return   : 
 Purpose  :
 Notes    : 
 ********************************************************************************/
eventhandler SENNHEISER_BEAMELEVATION(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_BEAMELEVATION]\n");
	#ENDIF

	Beam_Elevation = TCC2Sennheiser.gnBeamElevation; // Move the Simpl# data into Simpl+
}	
eventhandler SENNHEISER_BEAMAZIMUTH(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_BEAMAZIMUTH]\n");
	#ENDIF

	Beam_Azimuth = TCC2Sennheiser.gnBeamAzimuth; // Move the Simpl# data into Simpl+
}	
/********************************************************************************
 Function : LED 
 --------------------------------------------------------------------------------
 Arguments: 
 Output   : 
 Globals  : 
 Calls    : 
 Return   : 
 Purpose  :
 Notes    : 
 ********************************************************************************/
eventhandler SENNHEISER_LEDBRIGHTNESS(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_LEDBRIGHTNESS]\n");
	#ENDIF

	LED_Brightness = TCC2Sennheiser.gnLEDBrightness; // Move the Simpl# data into Simpl+
}	
/********************************************************************************
 Function : OSC Error 
 --------------------------------------------------------------------------------
 Arguments: 
 Output   : 
 Globals  : 
 Calls    : 
 Return   : 
 Purpose  :
 Notes    : 
 ********************************************************************************/
eventhandler SENNHEISER_OSCERROR(SennheiserTCC2Class sender, EventArgs e)
{
	#if_defined DEBUG
		print("[SENNHEISER_OSCERROR]\n");
	#ENDIF
	Errors = TCC2Sennheiser.OSCError;
}	     

/*******************************************************************************************
  Main()
  Uncomment and place one-time startup code here
  (This code will get called when the system starts up)
*******************************************************************************************/
Function Main()
{
    WaitForInitializationComplete();
	Init();
	gnProcessBuffer = FALSE;
	IsCommunicating = OFF;
}

                                  